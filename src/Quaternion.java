
/**
 * The class is originally destined for homework assignment no 4. The skeleton is
 * attributed to jpoial, src https://bitbucket.org/itc_algorithms/home4/
 * The solution is (c) of jeekim616 and his former incarnation furunkel027
 * Inspiration sources: in 2015 http://enos.itcollege.ee/~ylari/I231/Quaternion.java
 * and in 2019 obviously https://bitbucket.org/jeekim616/home4/ ===
 */


import java.util.Objects;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double re;
   private double ii;
   private double jj;
   private double kk;
   // And a constant representing "small enough" ;)
   static final double epsilon = 0.00000000001;


   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      re = a;
      ii = b;
      jj = c;
      kk = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.re;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart()
   {
      return this.ii;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.jj;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.kk;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
       // HEAVILY refactored to avoid code repetition
      StringBuilder conveyer = new StringBuilder();
      conveyer.append(convertParticle(this.re, 'r'));
      conveyer.append(convertParticle(this.ii, 'i'));
      conveyer.append(convertParticle(this.jj, 'j'));
      conveyer.append(convertParticle(this.kk, 'k'));
      String returnValue = conveyer.toString();
      returnValue = returnValue.replaceAll("\\s+", "");
      return returnValue;
   }

        /**
         * A helper function to be used by toString
         * @author jeekim616
         * @param partValue a Double particle to be converted
         * @param partName particle code
         * @return representation for this part of the Quaternion
         */
   static String convertParticle(double partValue, char partName)
   {
       String result = "";
       StringBuilder t = new StringBuilder();
       String tmp;
       char sign = ' ';
       if (partName != 'r') { // Generic part for for i,j,k
           // notate plus sign if any
           if (partValue >= 0) {
               sign = '+';
           } else
               sign = ' ';
           // notate the value of the particle
           tmp = Double.toString(partValue);
           t.append(sign);
           t.append(tmp);
           t.append(partName);
       } else { // handling Re is yet simpler
           tmp = Double.toString(partValue);
           t.append(tmp);
       }
       result = t.toString();
       return result;
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
       // Sanity checks first
      if (s.length() == 0) throw new IllegalArgumentException("EXCEPTION: an empty string resembles no quaternion") ;
       boolean minusSignReal = false, minusSignIndia = false;
       double r = 0. , i = 0. , j = 0. , k = 0. ;
       StringBuilder tt = new StringBuilder();
       String input = "";
       int count = 0;

       // Replace all "[i|j|k|[0-9]|.|+|-]" if length=0 then OK else illegal symbols
       input = s.replaceAll("[i|j|k|[0-9]|.|+|-]", "");
       count = input.length();
       if (count != 0) throw new IllegalArgumentException("EXCEPTION: " + count +
               " illegal symbols found in quaternion: " + "[" + s + "] Should be limited to[i|j|k|[0-9]|.|+|-]") ;

       // Count signs [+|-]. Must be 3 or 4
       String[] t = s.split("[-|+]");
       count = t.length; // we respectively get 4 or 5 splits
       count--; // one off so 3 or 4 signs possible
       if (s.charAt(0) == '-') {
            count--; // count of signs is normalised to 3 to ease the if condition
       }
       if (count != 3) {
           throw new IllegalArgumentException("EXCEPTION: an unrealistic number of signs in quaternion: [" + s + "]. Expected 3 or 4.");
       }

       String ss = s.trim();
       String s1, s2, s21, s22, s3, s4; // no reuse for clarity purposes

       // Parsing pattern :  (((-1.0-1.0) i -2.0) j +3.0) k
       //                           ^     ^       ^       ->
       // An alternative idea: first .split("[i|j|k]") and then .split("[-|+]")

       // Particle k
       String[] smth4 = ss.split("j");
       s4 = smth4[1];
       s4 = s4.substring(0, s4.length() - 1); // letter k -> away
       try {
           k = Double.parseDouble(s4);
       } catch (NumberFormatException e) {
           throw new RuntimeException("EXCEPTION: cannot convert particle k to double number in valueOf(): [" + s + "]");
       }
       // Particle j extracted, letter i thrown away as a delimiter
       String[] smth3 = smth4[0].split("i");
       s3 = smth3[1];

       try {
       j = Double.parseDouble(s3) ;
       } catch (NumberFormatException e) {
           throw new RuntimeException("EXCEPTION: cannot convert particle j to double number in valueOf(): [" + s + "]");
       }
       // A chemical factory to separate r and i parts
       s21 = smth3[0];
       s22 = "";

       if (s21.startsWith("-") ) {
           minusSignReal = true;
       }
       s22 = s21.replaceAll("^-", ""); // leading sign -> away

       if (s22.contains("-")) {
           minusSignIndia = true;
       }

       String[] smth12 = s22.split("[-|+]"); // sign of i as separator
       s1 = smth12[0];
       s2 = smth12[1];

       if (minusSignReal) {
           s1 = "-" + s1; // recover the sign for s1
       }
       try {
       r = Double.parseDouble(s1);
       } catch (NumberFormatException e) {
           throw new RuntimeException("EXCEPTION: cannot convert particle r to double number in valueOf(): [" + s + "]");
       }

       if (minusSignIndia) {
           s2 = "-" + s2; // recover the sign for s2
       }
       try {
       i = Double.parseDouble(s2);
       } catch (NumberFormatException e) {
           throw new RuntimeException("EXCEPTION: cannot convert particle i to double number in valueOf(): [" + s + "]");
       }

       Quaternion resultValue = new Quaternion(r, i, j, k);
       return resultValue;
   }


   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(this.re, this.ii, this.jj, this.kk);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      boolean indeed = false;
      // epsilon on konstant, vt ylevalpool
      double r = 0. , i = 0. , j = 0. , k = 0. ;

      r = Math.abs(this.re);
      i = Math.abs(this.ii);
      j = Math.abs(this.jj);
      k = Math.abs(this.kk);
      if (r <= epsilon) {
         if (i <= epsilon) {
            if (j <= epsilon) {
               if (k <= epsilon) {
                  indeed = true;
               }
            }
         }
      }
      return indeed;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
       double minusSign = -1. ;
       return new Quaternion(this.re, (this.ii * minusSign), (this.jj * minusSign), (this.kk * minusSign));
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
       double minusSign = -1. ;
       return new Quaternion((this.re * minusSign), (this.ii * minusSign), (this.jj * minusSign), (this.kk * minusSign));
   }

    /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
       return new Quaternion(this.re + q.re, this.ii + q.ii,
               this.jj + q.jj, this.kk + q.kk);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
       double r = 0. , i = 0. , j = 0. , k = 0. ;
       r = this.re * q.re - this.ii * q.ii - this.jj * q.jj - this.kk * q.kk;
       i = this.re * q.ii + this.ii * q.re + this.jj * q.kk - this.kk * q.jj;
       j = this.re * q.jj - this.ii * q.kk + this.jj * q.re + this.kk * q.ii;
       k = this.re * q.kk + this.ii * q.jj - this.jj * q.ii + this.kk * q.re;
       return new Quaternion( r, i, j, k);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
       return new Quaternion((this.re * r), (this.ii * r), (this.jj * r), (this.kk * r));
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
       if ( this.isZero() ) {
           throw new ArithmeticException("EXCEPTION: 1 being divided by ZERO is not kosher");
       }
       double r = 0. , i = 0. , j = 0. , k = 0. ;
       double minusSign = -1. ;

       r = this.re / (this.re * this.re + this.ii * this.ii
               + this.jj * this.jj + this.kk * this.kk);
       i = (this.ii * minusSign) / (this.re * this.re
               + this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
       j = (this.jj * minusSign) / (this.re * this.re
               + this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
       k = (this.kk * minusSign) / (this.re * this.re
               + this.ii * this.ii + this.jj * this.jj + this.kk * this.kk);
       return new Quaternion(r, i, j, k);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
       double r = 0. , i = 0. , j = 0. , k = 0. ;
       r = this.re - q.re;
       i = this.ii - q.ii;
       j = this.jj - q.jj;
       k = this.kk - q.kk;
       return new Quaternion(r, i, j, k);
   }

    /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
         if ( q.isZero() ) {
            throw new ArithmeticException("EXCEPTION: divideByRight: divide by ZERO is not kosher");
        }
       Quaternion inverted = q.inverse();
       return this.times(inverted);
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
       if ( q.isZero() ) {
           throw new ArithmeticException("EXCEPTION: divideByLeft: divide by ZERO is not kosher");
       }
       Quaternion inverted = q.inverse();
       return inverted.times(this);
   }

    /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
       if (!(qo instanceof Quaternion)) {
           return false;
       }
       // Meaningful operations follow
       double r = 0. , i = 0. , j = 0. , k = 0. ;
       boolean tr0 = false, tr1 = false, tr2 = false, tr3 = false;

       r = ( (Quaternion) qo).re;
       i = ( (Quaternion) qo).ii;
       j = ( (Quaternion) qo).jj;
       k = ( (Quaternion) qo).kk;

       if (Math.abs(this.re - r) < epsilon) { tr0 = true; }
       if (Math.abs(this.ii - i) < epsilon) { tr1 = true; }
       if (Math.abs(this.jj - j) < epsilon) { tr2 = true; }
       if (Math.abs(this.kk - k) < epsilon) { tr3 = true; }

       if (tr0 && tr1 && tr2 && tr3) {
           return true;
       }
       return false;

   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
       Quaternion tmp, addendA, addendB, sum, quotient ;
       double two = 2. ;
       tmp = q.conjugate();
       addendA = times(tmp);
       tmp = this.conjugate();
       addendB = q.times(tmp);
       sum = addendA.plus(addendB);
       quotient = new Quaternion (sum.re / two, sum.ii / two, sum.jj / two, sum.kk / two);
       return quotient;
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
       return Objects.hash(re, kk, ii, jj);
       // Anti vihje. Ise poleks selle peale tulnud.
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
       double returnValue = 0.;
       returnValue = Math.sqrt((this.re *this.re) + (this.ii * this.ii)
               + (this.jj * this.jj) + (this.kk * this.kk));
       return returnValue;
   }


    /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
